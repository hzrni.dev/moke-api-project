import { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router";

const EmployerList = () => {
  const [employees, setEmployees] = useState([]);
  const [isDeleted, setIsDeleted] = useState(0);
  const navigate = useNavigate();

  useEffect(() => {
    const controller = new AbortController();
    const fetch = async () => {
      try {
        const response = await axios.get("http://localhost:3000/employees", {
          signal: controller.signal,
        });
        setEmployees(response.data);
      } catch (error) {
        if (error.name === "AbortError") {
          console.log("Fetch aborted");
        }
      }
    };
    
    fetch();
 
    return () => {
      controller.abort();
    };
  }, [isDeleted]);

  const handleDelete = (id) => {
    axios.delete("http://localhost:3000/employees/" + id).then((response) => {
      console.log(response.data);
    });
    setIsDeleted(isDeleted + 1);
  };
  return (
    <>
      <button
        className="py-3 px-6 rounded-md shadow-md bg-emerald-400 mb-12 mx-8"
        onClick={() => navigate("/add-employee")}
      >
        Create new employee
      </button>
      <ul className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 mb-12 gap-6 px-8">
        {employees.map((employer) => (
          <li
            className="flex flex-col space-y-4 bg-slate-800 p-8 rounded-md shadow-md"
            key={employer.id}
          >
            <h4 className="text-xl text-emerald-400">
              {employer.first_name + " " + employer.last_name}
            </h4>
            <p className="flex flex-row space-x-2 items-center">
              <span className="text-base text-slate-400">{employer.email}</span>
              <span className="text-slate-200">|</span>
              <span className="text-base text-slate-400">
                {employer.gender.toLowerCase()}
              </span>
            </p>
            <span className="text-sm text-slate-100 flex-shrink-0 flex-grow-0 font-bold font-serif">
              {employer.status.toUpperCase()}
            </span>
            <div className="flex items-center space-x-3 ml-auto">
              <button
                className="py-1 px-2 bg-red-500 rounded-sm"
                onClick={() => handleDelete(employer.id)}
              >
                Delete
              </button>
              <button
                className="py-1 px-2 bg-blue-500 rounded-sm"
                onClick={() => navigate(`/edit-employee/${employer.id}`)}
              >
                Edit
              </button>
            </div>
          </li>
        ))}
      </ul>
    </>
  );
};

export default EmployerList;
