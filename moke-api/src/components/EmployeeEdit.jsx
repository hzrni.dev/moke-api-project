import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router";
const EmployeeEdit = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [info, setInfo] = useState({
    first_name: "",
    last_name: "",
    email: "",
    gender: "",
    status: "",
  });

  useEffect(() => {
    axios.get("http://localhost:3000/employees/" + id).then((res) => {
      setInfo(res.data);
      console.log(res.data);
    });
  }, []);
  const handleEdit = async (id) => {
    console.log(id);
    await axios
      .put("http://localhost:3000/employees/" + id, info)
      .then((res) => {
        console.log(res.data);
        navigate("/", { replace: true });
      });
  };
  return (
    <>
      <div className="p-2 mb-12 w-full">
        <h2 className="text-xl text-center mb-8 text-slate-100 font-bold">
          Edit Info
        </h2>
        <form
          action=""
          className="flex w-full flex-col items-center space-y-4"
          onSubmit={(e) => e.preventDefault()}
        >
          <div className="flex flex-col items-start w-2/3">
            <label className="text-slate-100">First Name:</label>
            <input
              value={info.first_name}
              onChange={(e) => setInfo({ ...info, first_name: e.target.value })}
              type="text"
              className="bg-slate-600 rounded-md outline-none border-2 border-emerald-400 text-slate-100 px-1 py-2 w-full"
            />
          </div>
          <div className="flex flex-col items-start w-2/3">
            <label className="text-slate-100">Last Name:</label>
            <input
              value={info.last_name}
              onChange={(e) => setInfo({ ...info, last_name: e.target.value })}
              type="text"
              className="bg-slate-600 rounded-md outline-none border-2 border-emerald-400 text-slate-100 px-1 py-2 w-full"
            />
          </div>
          <div className="flex flex-col items-start w-2/3">
            <label className="text-slate-100">Email:</label>
            <input
              value={info.email}
              onChange={(e) => setInfo({ ...info, email: e.target.value })}
              type="email"
              className="bg-slate-600 rounded-md outline-none border-2 border-emerald-400 text-slate-100 px-1 py-2 w-full"
            />
          </div>
          <div className="flex flex-col items-start w-2/3">
            <label className="text-slate-100">Gender:</label>
            <input
              value={info.gender}
              onChange={(e) => setInfo({ ...info, gender: e.target.value })}
              type="text"
              className="bg-slate-600 rounded-md outline-none border-2 border-emerald-400 text-slate-100 px-1 py-2 w-full"
            />
          </div>
          <div className="flex flex-col items-start w-2/3">
            <label className="text-slate-100">Status:</label>
            <input
              value={info.status}
              onChange={(e) => setInfo({ ...info, status: e.target.value })}
              type="text"
              className="bg-slate-600 rounded-md outline-none border-2 border-emerald-400 text-slate-100 px-1 py-2 w-full"
            />
          </div>
          <div className="flex-row items-center space-x-4">
            <button
              type="submit"
              className="bg-emerald-400 py-2 px-4 rounded-md"
              onClick={() => handleEdit(id)}
            >
              Confirm
            </button>
            <button
            type="button"
              className="bg-slate-100 py-2 px-4 rounded-md"
              onClick={() => navigate("/")}
            >
              Cancel
            </button>
          </div>
        </form>
      </div>
    </>
  );
};

export default EmployeeEdit;
