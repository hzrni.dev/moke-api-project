import EmployerList from './components/EmployersList';
import EmployeeAdd from './components/EmployeeAdd';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import EmployeeEdit from './components/EmployeeEdit';

function App() {

  return (
    <Router>

      <div className='w-full py-16 h-auto bg-slate-500'>
        <h2 className='text-center text-slate-100 text-xl mb-12 font-bold font-serif'>Json-Server With Axios</h2>
        
          <Routes>
            <Route path='/' element={<EmployerList/>}/>
            <Route path='/add-employee' element={<EmployeeAdd/>}/>
            <Route path='/edit-employee/:id' element={<EmployeeEdit/>}/>
          </Routes>
      </div>
    </Router>

  )
}

export default App;
